package ben.nso.server;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import static org.assertj.core.api.Assertions.assertThat;

import static org.hamcrest.CoreMatchers.*;


@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class HttpRequestTest {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void simpleSend() throws Exception {
		String response = this.restTemplate.getForObject("http://localhost:" + port + "/send?cId=1&cValue=123",
				String.class);
		assertThat(response == null);
	}

	@Test
	public void invalidParams() throws Exception {
		String stringResponse = this.restTemplate.getForObject("http://localhost:" + port + "/send?cId=1&cValue=-123",
				String.class);
		JSONObject response = new JSONObject(stringResponse);
		Assert.assertThat(response.getInt("status"), is(400));
		Assert.assertThat(response.getString("error"), is("Bad Request"));
		Assert.assertThat(response.getString("message"), is("invalid parameter value: cValue must be positive"));
	}



}