package ben.nso.server;

public interface Validator {
    void validate(Integer cId, Integer cValue);
}
