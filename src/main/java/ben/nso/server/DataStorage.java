package ben.nso.server;

import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class DataStorage {

    ConcurrentHashMap clientsValues;

    @PostConstruct
    private void init(){
        clientsValues = new ConcurrentHashMap();

    }

    public  void  store(Integer cId, Integer cValue){

        clientsValues.putIfAbsent(cId,new TreeSet<Integer>());

        TreeSet<Integer> set= (TreeSet<Integer>) clientsValues.get(cId);
        synchronized (set){
            set.add(cValue);
        }
    }

    public  Map getAll(){

        return clientsValues;
    }

    public  Object getClientValues(int cId){
        return clientsValues.get(cId);
    }

}
