package ben.nso.server;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class SendController {

    @Autowired
    private SendValidator sendValidator;

    @Autowired
    private DataStorage dataStorage;



    @RequestMapping("/send")
    public void send(@RequestParam Integer cId, @RequestParam Integer cValue){
        sendValidator.validate(cId, cValue);
        dataStorage.store(cId,cValue);

    }

    @RequestMapping("/getAll")
    public Map getAll()
    {
        return dataStorage.getAll();
    }

    @RequestMapping("/get/{cId}")
    public Object getAll(@PathVariable("cId") int cId){
        return dataStorage.getClientValues(cId);

    }






}
