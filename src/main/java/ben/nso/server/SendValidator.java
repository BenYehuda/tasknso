package ben.nso.server;

import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.TOO_MANY_REQUESTS;

@Service
public class SendValidator implements Validator {
    public static final int MaxReqPerTimeFrame = 5;
    private ConcurrentHashMap<Integer, BlockingQueue> last5RequestTimeStempPerValue;
    private Lock mapLock;

    @PostConstruct
    private void init(){
        mapLock = new ReentrantLock();
        last5RequestTimeStempPerValue = new ConcurrentHashMap<>();
    }


    public void validate(Integer cId, Integer cValue) {
        validateInput(cId, cValue);
        validateRateLimit(cId);
    }

    private void validateInput(Integer cId, Integer cValue) {
        if(cId== null || cValue== null){
            throw new ResponseStatusException(BAD_REQUEST,"missing cId or cValue params");
        }
        if(cValue <0){
            throw new ResponseStatusException(BAD_REQUEST,"invalid parameter value: cValue must be positive");
        }
    }

    private void validateRateLimit(Integer cId) {
         last5RequestTimeStempPerValue.putIfAbsent(cId, new ArrayBlockingQueue<LocalDateTime>(MaxReqPerTimeFrame));

         synchronized (last5RequestTimeStempPerValue.get(cId)) {
            BlockingQueue cIdLastRequests = last5RequestTimeStempPerValue.get(cId);
            LocalDateTime now = LocalDateTime.now();
            if (cIdLastRequests.size() < MaxReqPerTimeFrame) {
                cIdLastRequests.add(now);
            }
            else{
                LocalDateTime last= (LocalDateTime) cIdLastRequests.poll();
                long timeFromFirst = ChronoUnit.SECONDS.between(last, now);
                cIdLastRequests.add(now);
                if(timeFromFirst<10){
                    throw new ResponseStatusException(TOO_MANY_REQUESTS);
                }
            }
        }
    }
}
